import React, { Component } from 'react';
import {storeProducts,detailProduct} from './data';
import { thisExpression } from '@babel/types';
const ProductContext =React.createContext();




class ProductProvider extends Component {

    state={
        products:storeProducts,
        detailProduct:detailProduct,
        cart:[],
        modalOpen:false,
        modalProduct:detailProduct,
      
    }

    componentDidMount(){
        this.setProducts();
    }
    


    setProducts =()=>{
        let tempProdcuts = [];
        storeProducts.forEach(item =>{
            const singleItem ={...item};
            tempProdcuts=[...tempProdcuts,singleItem];
        })
        this.setState(()=>{
            return{products:tempProdcuts};
        })

        console.log(`temp products ${tempProdcuts}`)
    }

    getItem = id =>{
        const product = this.state.products.find(item =>item.id === id);
        return product;
    }
    handleDetail=(id)=>{
        const product = this.getItem(id);
        this.setState(()=>{
            return {
                detailProduct:product,
                flagmodel:true
            }
            
        })
    };
    
    addToCart=id=>{
       let tempProdcuts =  [...this.state.products];
       const index = tempProdcuts.indexOf(this.getItem(id));
       const product = tempProdcuts[index];
       console.log(`this what we get through index of ${index}`)
       product.inCart=true;
       product.count=1;
       const price = product.price;
       product.total = price;
       this.setState(()=>{
           return{
               product:tempProdcuts,
               cart:[...this.state.cart,product]
               
        
           }
       })

    }

    openModal = id =>{
        const product = this.getItem(id);
        this.setState(()=>{
            return{
                modalProduct:product,
                modalOpen:true
            }
            
        })
    }

    closeModal = () =>{
        this.setState(()=>{
            return{
                modalOpen:false
            }
        })

    }

   


    render() {
        console.log(this.state.products)
        console.log("this is cart",this.state.cart)
        
        return (
           <ProductContext.Provider value={{
             ...this.state,
             handleDetail:this.handleDetail,
             addToCart:this.addToCart,   
             openModal:this.openModal,
             closeModal:this.closeModal

           }}>
          
               {this.props.children}
           </ProductContext.Provider>
        );
    }
}
const ProductConsumer = ProductContext.Consumer;

export {ProductProvider,ProductConsumer};