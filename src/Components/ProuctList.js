import React, { Component } from 'react';
import Product from './Product';
import Title from './Title';
import {storeProducts} from '../data';
import {ProductConsumer} from '../context';

class ProuctList extends Component {


    render() {
        
        return (
            <div>
                <React.Fragment>
                    <div className="py-5">
                        <div className="container">
                            <Title name="our"  title="Products"/>
                            <div className ="row">
                            <ProductConsumer>
                                {(UsedAsVariable)=> {
                                    return UsedAsVariable.products.map(product =>{
                                        return <Product key={product.id} product ={product} ></Product>
                                    })
                                }}

                            </ProductConsumer>

                            
                            
                            
                            </div>

                        </div>
                    
                    </div>


                </React.Fragment>
                

                {/* <Product/> */}


            </div>
        );
    }
}

export default ProuctList;