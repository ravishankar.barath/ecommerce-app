import React, { Component } from 'react';

import {ProductConsumer} from '../context';
import {Link} from 'react-router-dom';
import styled from 'styled-components';

import Modal from 'react-responsive-modal';

class Details extends Component {
    render() {
        return (
           <ProductConsumer>
               {(value)=>{
                  const{id,title,company,img,info,price,inCart}=
                   value.detailProduct;
                return(
                    
                    <div className="container py-5">
                    {/* title */}
                    <div className="row">
                        <div className="col-10 mx-auto col-md-6 my-3">
                            <img src={img}></img>                        
                            <h2>Model:{title}</h2>
                            <h4 className="text-title text-uppercase text-muted mt-3 mb-2">
                                made by: <span className="text-uppercase">
                                {company}
                                </span>
                            </h4>

                            <h4 className="text-blue">
                                <strong>
                                    price:<span>$</span>
                                    {price}
                                </strong>

                                <p className="text-capitalize font-weight-bold mt-3 mb-0">
                                some info about product:
                                </p>
                                <p className="text-muted lead">{info}</p>
                            </h4>

                            <Link to='/'><ButtonContainer>Back to Product</ButtonContainer></Link>
                            
                            <ButtonContainer1 disabled={inCart?true:false}
                            onClick={()=>{
                                value.addToCart(id);
                                value.openModal(id);
                            }}>
                                {inCart ?"inCart":"add to cart"}
                            </ButtonContainer1>

                        </div>
                        </div>
                        

                   
                    </div>
                )
               }}
           </ProductConsumer>
        );
    }
}

export default Details;

const ButtonContainer = styled.button `
text-transform:capitalize;
font-size:1.4rem;
background:transparent;
border:0.05rem solid var(--lightBlue);
color:var(--lightBlue);
border-radius:0.5rem;

padding:0.2rem 0.5rem;
cursor:pointer;
margin:0.2rem 0.5rem 0.2rem 0;
transition: all 0.5s ease-in-out;
&:hover{ 
    background:var(--lightBlue);
    color:var(--mainBlue);
}
&:focus{
    outline:none;
}

`;

const ButtonContainer1 = styled.button `
text-transform:capitalize;
font-size:1.4rem;
background:transparent;
border:0.05rem solid var(--mainYellow);
color:var(--mainYellow);
border-radius:0.5rem;

padding:0.2rem 0.5rem;
cursor:pointer;
margin:0.2rem 0.5rem 0.2rem 0;
transition: all 0.5s ease-in-out;
&:hover{ 
    background:var(--mainYellow);
    color:var(--mainBlack);
}
&:focus{
    outline:none;
}

`;